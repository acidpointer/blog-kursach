const express = require('express');

const router = express.Router();

const mainController = require('../controllers/main');

router.get('/', mainController.main_index_get);
router.get('/about', mainController.main_about_get);
router.get('/auth', mainController.main_auth_get);
router.post('/auth', mainController.main_auth_post);
router.get('/logout', mainController.main_logout_get);

module.exports = router;