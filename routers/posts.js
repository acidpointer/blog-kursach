const express = require('express');

const router = express.Router();

const postsController = require('../controllers/posts');

router.get('/:code', postsController.posts_page_get);
router.get('/c/bulk', postsController.posts_bulk_get);

module.exports = router;