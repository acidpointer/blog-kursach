const { DataTypes, Sequelize } = require('sequelize');
const db = require('../db');

module.exports = db.define('post', {
    uuid: {
        type: DataTypes.UUIDV4,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
    },
    code: {
        type: DataTypes.INTEGER,
        unique: true,
        allowNull: false
    },
    title: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    preview: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false
    }
});