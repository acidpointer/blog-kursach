const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './DATA.db'
});

/**
 * Test connection with database!
 */
sequelize.authenticate().then(() => {
    console.log('Connection with database established!');
}).catch(err => {
    console.log(`Could not connect to database: {${err}}`);
});

module.exports = sequelize;