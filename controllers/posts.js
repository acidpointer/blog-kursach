const { customAlphabet } = require('nanoid');
const nanoid = customAlphabet('0123456789', 8);

const postModel = require('../models/post.model');

exports.posts_bulk_get = async function(req, res) {
    postModel.bulkCreate([
        {
            code: nanoid(),
            title: 'Индийский инженер в иске к Cisco утверждает, что 90% IT-работников из Индии в США принадлежат к высшей касте',
            preview: `
Инженер Джон Доу, который на протяжении 20 лет разрабатывал программное обеспечение в Cisco, 
обратился с иском к компании. Индус, который относится к касте неприкасаемых, заявил, что 
крупные технологические компании Кремниевой долины допускают в своих рядах открытую кастовую 
дискриминацию. По его словам, более 90% индийских технарей в США являются представителями высшей касты.`,
            content: `
Инженер Джон Доу, который на протяжении 20 лет разрабатывал программное обеспечение в Cisco, обратился с иском к компании. 
Индус, который относится к касте неприкасаемых, заявил, что крупные технологические компании Кремниевой долины допускают в 
своих рядах открытую кастовую дискриминацию. По его словам, более 90% индийских технарей в США являются представителями высшей касты.
Автор иска пояснил, что он был выпускником Индийского технологического института. Его коллега окончил тот же институт, но при этом он вырос 
в Бомбее как представитель высшей касты. Узнав Доу, он якобы немедленно начал высмеивать его перед другими индийскими служащими из 
высшей касты, которые работали в Cisco. Так, к примеру, он заявил, что Доу поступил в инженерный институт только благодаря правительственным 
действиям по поддержке низших каст в 80-е.
Когда Доу сообщил отделу кадров Cisco, что хочет подать жалобу, там ему якобы сказали, что «кастовая дискриминация не является незаконной». 
Вскоре после этого он был понижен с главной роли в двух проектах. В иске говорится, что в течение двух лет продолжалась травля Доу со стороны 
старшего коллеги. Тот не давал ему никаких бонусов и лишил возможности продвижения по службе.
Затем один начальник Доу сменился другим, тоже инженером высшей касты, окончившим тот же институт в Индии. И тогда травля, по его словам, продолжилась.
В Cisco категорически отрицают обвинения индуса. Там заявили, что компания «стремится к созданию рабочих мест для всех», 
а также имеет «надежные процессы для сообщения о проблемах и их расследования».
Однако данный судебный процесс вызвал целую волну жалоб технарей-неприкасаемых, которые рассказали о своем преследовании 
в США со стороны представителей высших каст. По меньшей мере 250 человек, работающих в таких компаниях, как Google, Facebook, 
Microsoft, Apple и Netflix, сообщили о случаях преследований, унижений, издевательств и вмешательств, мешающих карьере. 
            `
        },
        {
            code: nanoid(),
            title: 'Linux Journal возвращается. Теперь под руководством Slashdot Media',
            preview: `
22 сентября 2020 года новая редакция Linux Journal объявила о возобновлении работы журнала. 
Закрытый год назад информационный ресурс, посвященный Linux, получил финансирование и переходит под руководство компании Slashdot Media.
            `,
            content: `
22 сентября 2020 года новая редакция Linux Journal объявила о возобновлении работы журнала. Закрытый год назад информационный ресурс, посвященный Linux, 
получил финансирование и переходит под руководство компании Slashdot Media.
Linux Journal будет полноценно работать и поддерживаться начиная с этого момента и на неопределенный срок. 
В скором времени в журнале будет размещен новый контент.
Редакция просит связаться с ними всех бывших участников Linux Journal или энтузиастов Linux, 
которые хотят вновь или впервые принять участие в этом проекте.
Редакция пока что не имеет планов по реализации схем подписки на издание, весь новый контент будет публиковаться бесплатно.
Компания Slashdot Media владеет и управляет новостным техническим сайтом Slashdot и порталом для 
разработчиков открытого программного обеспечения SourceForge.
Ранее 7 августа 2019 года Linux Journal объявил о закрытии спустя почти 30 лет работы. 
Все сотрудники были уволены, у издания не осталось средств для продолжения работы в каком-либо виде. 
Веб-сайт издания продолжил функционировать в качестве архива.
            `
        }
    ]).then(() => {
        console.log(`Bulk posts created!`);
        res.redirect('/');
    }).catch(err => {
        console.log(`Can't create bulk posts! {${err}}`);
    });
};

exports.posts_page_get = async function(req, res) {
    let postCode = req.params.code;

    postModel.findOne({
        where: {
            code: postCode
        }
    }).then(post => {
        res.render('post.html', {
            title: `ID:${post.code}`,
            logged_in: req.session.logged_in,
            post: post
        });
    }).catch(err => {
        console.log(`Cant't render post with code [${postCode}] {${err}}`);
    });
};