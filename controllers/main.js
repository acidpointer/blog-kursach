const postModel = require('../models/post.model');

const USER = 'admin';
const PASS = 'nobody';


exports.main_index_get = async function(req, res) {
    res.render('index', {
        logged_in: req.session.logged_in,
        posts: await postModel.findAll(),
        title: 'Главная'
    });
};

exports.main_about_get = async function(req, res) {
    res.render('about', {
        logged_in: req.session.logged_in,
        title: 'Авторы'
    });
};

exports.main_auth_get = async function(req, res) {
    res.render('auth', {
        logged_in: req.session.logged_in,
        title: 'Авторизация'
    });
};

exports.main_auth_post = async function(req, res) {
    if (req.body.login === USER && req.body.password === PASS) {
        console.log(`Authenticated!`);
        req.session.logged_in = true;
        res.redirect('/');
    }
}

exports.main_logout_get = async function(req, res) {
    req.session.logged_in = false;
    res.redirect('/');
}