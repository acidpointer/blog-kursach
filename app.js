const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const expressNunjucks = require('express-nunjucks');
const app = express();


const session = require('express-session');
const cookieParser = require('cookie-parser');
app.use(session({
    secret: 'SECRETKEYGENERATEDHEREWITHOPENSSHORANYotherMAyBePGPorS0M3th1NG3lS3'
}));
app.use(cookieParser());

// let's parse HTTP POST body
app.use(bodyParser.urlencoded({ extended: true }));

// CONFIG
const port = 8000;
const dev = true;

const njk = expressNunjucks(app, {
    watch: dev,
    noCache: dev
});


app.set('views', path.join(__dirname, 'views'));


const models = [
    require('./models/post.model'),
]

models.forEach(model => {
    model.sync().catch(err => {
        console.log(`Can't sync model! Error: {${err}}`);
    });
});

app.use('/static', express.static('public'));

app.use('/', require('./routers/main'));
app.use('/post', require('./routers/posts'));

app.listen(port, () => {
    console.log(`Started at port :${port}`);
});